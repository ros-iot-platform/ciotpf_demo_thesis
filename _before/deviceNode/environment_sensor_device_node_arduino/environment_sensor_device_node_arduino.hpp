#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>


void temperature_publish(std_msgs::Float32* msg, void* arg);
void luminosity_publish(std_msgs::Float32* msg, void* arg);

    
class EnvironmentSensorDeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "environment_sensor_device_node";
  
  static constexpr int Temperature_PUBLISH_RATE = 1000;
  static constexpr int Luminosity_PUBLISH_RATE = 1000;

  public:
    
    ros2::Publisher<std_msgs::Float32>* temperature_publisher = nullptr;
    ros2::Publisher<std_msgs::Float32>* luminosity_publisher = nullptr;
    
     
    EnvironmentSensorDeviceNode(): Node(NODE_NAME) {
      
      temperature_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/environment_sensor/sensor/temperature");
      this->createWallTimer(Temperature_PUBLISH_RATE, (ros2::CallbackFunc)temperature_publish, nullptr, temperature_publisher);
      luminosity_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/environment_sensor/sensor/luminosity");
      this->createWallTimer(Luminosity_PUBLISH_RATE, (ros2::CallbackFunc)luminosity_publish, nullptr, luminosity_publisher);
          
    }
    
};

EnvironmentSensorDeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
