#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>


void light_status_publish(std_msgs::Bool* msg, void* arg);

void light_control_subscriber_callback(std_msgs::Bool* msg, void* arg);
    
class CeilingLightDeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "ceiling_light_device_node";
  
  static constexpr int LightStatus_PUBLISH_RATE = 5000;

  public:
    
    ros2::Publisher<std_msgs::Bool>* light_status_publisher = nullptr;
    
    ros2::Subscriber<std_msgs::Bool>* light_control_subscriber = nullptr;
     
    CeilingLightDeviceNode(): Node(NODE_NAME) {
      
      light_status_publisher = this->createPublisher<std_msgs::Bool>("ciotpf/device/ceiling_light/sensor/light_status");
      this->createWallTimer(LightStatus_PUBLISH_RATE, (ros2::CallbackFunc)light_status_publish, nullptr, light_status_publisher);
      
      this->createSubscriber<std_msgs::Bool>("ciotpf/device/ceiling_light/actuator/light_control", (ros2::CallbackFunc)light_control_subscriber_callback, nullptr);    
    }
    
};

CeilingLightDeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
