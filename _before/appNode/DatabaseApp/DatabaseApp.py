import rclpy
from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
from libs._DatabaseApp import _DatabaseAppAppNode
from typing import Any

class DatabaseAppAppNode(_DatabaseAppAppNode):

    
    


    def __init__(self):
        #親のinitを最初に呼ぶ
        super().__init__()
        ##

    
    
    

    
   #全てのトピック 
    def all_topics_subscription_callback(self, topicName: str, message: Any) -> None: 
        raise NotImplementedError() 

    



def main(args=None):
    rclpy.init(args=args)

    node = DatabaseAppAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()