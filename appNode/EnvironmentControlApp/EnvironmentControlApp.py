import rclpy
from std_msgs.msg import Bool, String, Float32, Int32
from diagnostic_msgs.msg import KeyValue
from libs._EnvironmentControlApp import _EnvironmentControlAppAppNode
from typing import Any

class EnvironmentControlAppAppNode(_EnvironmentControlAppAppNode):

    
    
    #照明の操作 
    #def light_publish(self, value: Bool) -> None:

    #温度の設定 
    #def temperature_control_publish(self, value: Int32) -> None:



    def __init__(self):
        #親のinitを最初に呼ぶ
        super().__init__()
        ##

    
    
    
   #現在の温度 
    def temperature_subscription_callback(self, topicName: str, message: Float32) -> None: 
        msg = Int32()

        if message.data > 30:
            msg.data = 25 
            self.light_publish(msg)
        else:
            msg.data = 20
            self.light_publish(msg)


   #現在の照度 
    def luminosity_subscription_callback(self, topicName: str, message: Float32) -> None: 
        msg = Bool()

        if message.data > 400.0:
            msg.data = False
            self.light_publish(msg)
        else:
            msg.data = True
            self.light_publish(msg)


    
    



def main(args=None):
    rclpy.init(args=args)

    node = EnvironmentControlAppAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()