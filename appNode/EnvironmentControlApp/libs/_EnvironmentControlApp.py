
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from std_msgs.msg import Bool, String, Float32, Int32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Callable,TypeVar, Optional, Any
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _EnvironmentControlAppAppNode(CiotpfAppNodeBase):

    APP_NAME = "environment_control_app"
    NODE_NAME = "environment_control_app_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME, self.APP_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
            
        ]
        self.manual_subscriptions= [
            
            ManualSubscriptionDataModel("Temperature", "/ciotpf/config/app/environment_control_app/manual_topics/subscribe/temperature", False, self.temperature_subscription_callback, Float32)
            ManualSubscriptionDataModel("Luminosity", "/ciotpf/config/app/environment_control_app/manual_topics/subscribe/luminosity", False, self.luminosity_subscription_callback, Float32)
        ]
        
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            
        ]
        self.manual_publushers: List[ManualPublisherDataModel] = [
            
            ManualPublisherDataModel("Light", "/ciotpf/config/app/environment_control_app/manual_topics/publish/light", True, Bool)
            ManualPublisherDataModel("TemperatureControl", "/ciotpf/config/app/environment_control_app/manual_topics/publish/temperature_control", False, Int32)
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)


    # TIMER_PERIODの間隔で勝手に呼ばれる
    def _periodic_task(self) -> None:
        super()._periodic_task()

        
    
        
   #現在の温度 
    def temperature_subscription_callback(self, topicName: str, message: Float32) -> None: 
        raise NotImplementedError() 

   #現在の照度 
    def luminosity_subscription_callback(self, topicName: str, message: Float32) -> None: 
        raise NotImplementedError() 


        

        

        
    #照明の操作 
    def light_publish(self, value: Bool) -> None:
        pub: List[ManualPublisherDataModel] = [pub for pub in self.manual_publushers if pub.name == "Light"]
        if len(pub) != 1: 
            raise Exception("WHAAAA") 
        self._publish(pub[0], value)
 
    #温度の設定 
    def temperature_control_publish(self, value: Int32) -> None:
        pub: List[ManualPublisherDataModel] = [pub for pub in self.manual_publushers if pub.name == "TemperatureControl"]
        if len(pub) != 1: 
            raise Exception("WHAAAA") 
        self._publish(pub[0], value)
 

