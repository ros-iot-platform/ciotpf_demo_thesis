#include "environment_sensor_device_node_arduino.hpp"

#define SSID       "aterm-154cb3-g"
#define SSID_PW    "4b2854c0731d7"
#define AGENT_IP   "192.168.10.102"
#define AGENT_PORT 2018

/*
  Pins
*/
const uint8_t SCLK_bme280 = 14;
const uint8_t MOSI_bme280 =13; //Master Output Slave Input ESP32=Master,BME280=slave 
const uint8_t MISO_bme280 =12; //Master Input Slave Output
const uint8_t CS_bme280 = 26; //CS pin

const uint8_t SDA_M1750 = 21; 
const uint8_t SCL_M1750 = 22; 

int LED_BUILTIN = 32;


/*------------------------------------------
変数
------------------------------------------*/
ESP32_BME280_SPI bme280spi(SCLK_bme280, MOSI_bme280, MISO_bme280, CS_bme280, 10000000);
BH1750 bh1750;

float temperature;
float lux;


WiFiUDP udp;
void setup() 
{
  Serial.begin(9600);
  WiFi.begin(SSID, SSID_PW);
  while(WiFi.status() != WL_CONNECTED);
  ros2::init(&udp, AGENT_IP, AGENT_PORT);
  node = new EnvironmentSensorDeviceNode();
  xTaskCreatePinnedToCore(MainThread, "MainTheread", 1024*16, NULL, 2, nullptr, 1);



  /*
    Setup sensors
  */
  uint8_t t_sb = 5; //stanby 1000ms
  uint8_t filter = 2; //filter 
  uint8_t osrs_t = 4; //OverSampling Temperature x4
  uint8_t osrs_p = 4; //OverSampling Pressure x4
  uint8_t osrs_h = 4; //OverSampling Humidity x4
  uint8_t Mode = 3; //Normal mode
 
  bme280spi.ESP32_BME280_SPI_Init(t_sb, filter, osrs_t, osrs_p, osrs_h, Mode);

  Wire.begin(SDA_M1750, SCL_M1750);
  
  if(bh1750.begin(BH1750::ONE_TIME_HIGH_RES_MODE, 0x23, &Wire)) {
    Serial.println(F("BH1750 begin"));
  }
  else {
    Serial.println(F("Error initialising BH1750"));
    ESP.restart();
  }

  pinMode (LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);

}


void main_loop() {
  delay(1000);
}




//温度センサの値
//1000msecごとに実行される
//msg->data = 10.12 
void temperature_publish(std_msgs::Float32* msg, void* arg){ 
  temperature = bme280spi.Read_Temperature();
  msg->data = temperature;
}

//照度センサの値(lux)
//1000msecごとに実行される
//msg->data = 10.12 
void luminosity_publish(std_msgs::Float32* msg, void* arg){ 
  lux = bh1750.readLightLevel();
  msg->data = lux;
}

