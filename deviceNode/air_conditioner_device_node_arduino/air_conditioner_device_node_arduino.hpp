#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>


void temperature_status_publish(std_msgs::Int32* msg, void* arg);

void temperature_setting_subscriber_callback(std_msgs::Int32* msg, void* arg);
    
class AirConditionerDeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "air_conditioner_device_node";
  
  static constexpr int TemperatureStatus_PUBLISH_RATE = 5000;

  public:
    
    ros2::Publisher<std_msgs::Int32>* temperature_status_publisher = nullptr;
    
    ros2::Subscriber<std_msgs::Int32>* temperature_setting_subscriber = nullptr;
     
    AirConditionerDeviceNode(): Node(NODE_NAME) {
      
      temperature_status_publisher = this->createPublisher<std_msgs::Int32>("ciotpf/device/air_conditioner/sensor/temperature_status");
      this->createWallTimer(TemperatureStatus_PUBLISH_RATE, (ros2::CallbackFunc)temperature_status_publish, nullptr, temperature_status_publisher);
      
      this->createSubscriber<std_msgs::Int32>("ciotpf/device/air_conditioner/actuator/temperature_setting", (ros2::CallbackFunc)temperature_setting_subscriber_callback, nullptr);    
    }
    
};

AirConditionerDeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
