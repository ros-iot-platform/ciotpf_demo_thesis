#include "ceiling_light_device_node_arduino.hpp"

#define SSID       "aterm-154cb3-g"
#define SSID_PW    "4b2854c0731d7"
#define AGENT_IP   "192.168.10.102"
#define AGENT_PORT 2018

WiFiUDP udp;
void setup() 
{
  Serial.begin(9600);
  WiFi.begin(SSID, SSID_PW);
  while(WiFi.status() != WL_CONNECTED);
  ros2::init(&udp, AGENT_IP, AGENT_PORT);
  node = new CeilingLightDeviceNode();
  xTaskCreatePinnedToCore(MainThread, "MainTheread", 1024*16, NULL, 2, nullptr, 1);

}


void main_loop() {
  delay(1000);
}


bool currentValue = false;

//照明の状態
//5000msecごとに実行される
//msg->data = false 
void light_status_publish(std_msgs::Bool* msg, void* arg){ 
  msg->data = currentValue;
}


//照明のオンオフ
//bool value = msg->data; 
void light_control_subscriber_callback(std_msgs::Bool* msg, void* arg){ 
  currentValue = msg->data;
}